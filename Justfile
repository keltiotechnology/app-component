[private]
default:
  @just --list --unsorted

template-local app:
  helm template ./{{app}} --debug -f ./{{app}}/test/values.yaml

template-kubectl app:
  helm template ./{{app}} --debug -f ./{{app}}/test/values.yaml | kubectl apply -f - --dry-run=server
