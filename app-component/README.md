# app-component

![Version: 3.14.0](https://img.shields.io/badge/Version-3.14.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 1.0.1](https://img.shields.io/badge/AppVersion-1.0.1-informational?style=flat-square)

A Helm chart for Kubernetes

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://charts.bitnami.com/bitnami | postgresql | 12.1.2 |
| https://charts.bitnami.com/bitnami | redis | 17.3.11 |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| affinity | object | `{}` |  |
| autoscaling.enabled | bool | `false` |  |
| autoscaling.maxReplicas | int | `10` |  |
| autoscaling.minReplicas | int | `1` |  |
| autoscaling.scaleDown.percentPeriodSeconds | int | `60` | The period in seconds over which the percentage is calculated |
| autoscaling.scaleDown.percentValue | string | `nil` | The percentage to scale down by (set to null to use podsValue instead) |
| autoscaling.scaleDown.podsPeriodSeconds | int | `60` | The period in seconds over which the number of pods is calculated |
| autoscaling.scaleDown.podsValue | int | `2` | The number of pods to remove in one scaling operation (set to null to use percentage instead) |
| autoscaling.scaleDown.selectPolicy | string | `"Max"` | Select the policy to use for scaling: Max, Min or Disabled |
| autoscaling.scaleDown.stabilizationWindowSeconds | int | `300` | The time (in seconds) the HPA waits before scaling down |
| autoscaling.scaleUp.percentPeriodSeconds | int | `60` | The period in seconds over which the percentage is calculated |
| autoscaling.scaleUp.percentValue | string | `nil` | The percentage to scale up by (set to null to use podsValue instead) |
| autoscaling.scaleUp.podsPeriodSeconds | int | `60` | The period in seconds over which the number of pods is calculated |
| autoscaling.scaleUp.podsValue | int | `4` | The number of pods to add in one scaling operation (set to null to use percentage instead) |
| autoscaling.scaleUp.selectPolicy | string | `"Max"` | Select the policy to use for scaling: Max, Min or Disabled |
| autoscaling.scaleUp.stabilizationWindowSeconds | int | `60` | The time (in seconds) the HPA waits before scaling up |
| autoscaling.targetCPUUtilizationPercentage | int | `80` | Target CPU utilization percentage |
| autoscaling.targetMemoryUtilizationPercentage | int | `75` | Target Memory utilization percentage |
| configmaps | list | `[]` |  |
| containers[0].env | list | `[]` |  |
| containers[0].image.pullPolicy | string | `"IfNotPresent"` |  |
| containers[0].image.repository | string | `"nginx"` |  |
| containers[0].image.tag | string | `"latest"` |  |
| containers[0].livenessProbe | string | `nil` |  |
| containers[0].name | string | `"my-container"` |  |
| containers[0].ports | list | `[]` |  |
| containers[0].readinessProbe | string | `nil` |  |
| containers[0].securityContext | object | `{}` |  |
| containers[0].volumeMounts | list | `[]` |  |
| cronjobs | list | `[]` |  |
| cronjobsDefaultImage | object | `{}` |  |
| daemonset.annotations | object | `{}` |  |
| daemonset.enabled | bool | `false` |  |
| deployment.annotations | object | `{}` |  |
| deployment.enabled | bool | `true` |  |
| dnsConfig | object | `{}` | Use this option to set a custom DNS configurations to the created containers |
| dnsPolicy | string | `""` | Use this option to set a custom DNS policy to the created containers |
| env | list | `[]` |  |
| envFrom | list | `[]` |  |
| fullnameOverride | string | `""` |  |
| imagePullSecrets | list | `[]` |  |
| ingress.annotations | object | `{}` |  |
| ingress.className | string | `""` |  |
| ingress.enabled | bool | `false` |  |
| ingress.hosts[0].host | string | `"chart-example.local"` |  |
| ingress.hosts[0].paths[0].backend | string | `nil` | By default, it uses `services.ports[0]` |
| ingress.hosts[0].paths[0].path | string | `"/"` |  |
| ingress.hosts[0].paths[0].pathType | string | `"ImplementationSpecific"` |  |
| ingress.labels | object | `{}` |  |
| ingress.tls | list | `[]` |  |
| initContainers | list | `[]` |  |
| monitoring.blackboxExporterEndpoint | string | `"blackbox-exporter-prometheus-blackbox-exporter:9115"` |  |
| monitoring.enabled | bool | `true` |  |
| nameOverride | string | `""` |  |
| nodeSelector | object | `{}` | Select the node to put the `.Values.pod.kind` into |
| persistentVolumeClaims | list | `[]` |  |
| podAnnotations | object | `{}` |  |
| podSecurityContext | object | `{}` |  |
| postgresql.enabled | bool | `false` |  |
| redis.enabled | bool | `false` |  |
| replicaCount | int | `1` |  |
| resources | object | `{}` |  |
| roleBindings | list | `[]` |  |
| roles | list | `[]` |  |
| secrets | list | `[]` |  |
| service.annotations | object | `{}` |  |
| service.enabled | bool | `true` |  |
| service.labels | object | `{}` |  |
| service.ports[0].name | string | `"http"` |  |
| service.ports[0].port | int | `80` |  |
| service.ports[0].protocol | string | `"TCP"` |  |
| service.ports[0].targetPort | int | `8080` |  |
| service.type | string | `"ClusterIP"` |  |
| serviceAccount.annotations | object | `{}` | Annotations to add to the service account |
| serviceAccount.create | bool | `true` | Specifies whether a service account should be created |
| serviceAccount.name | string | `""` | The name of the service account to use. If not set and create is true, a name is generated using the fullname template |
| tests.enabled | bool | `false` |  |
| tolerations | list | `[]` |  |
| topologySpreadConstraints | list | `[]` |  |
| volumes | list | `[]` |  |

----------------------------------------------
Autogenerated from chart metadata using [helm-docs v1.14.2](https://github.com/norwoodj/helm-docs/releases/v1.14.2)
